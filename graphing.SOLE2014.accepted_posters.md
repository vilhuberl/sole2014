SOLE 2014 as a picture
======================




Or: the power of **evidence**.

A picture is worth a thousand words. Or in this case 1183 words - the number of distinct words in titles of the 343 papers accepted (either for a session or a poster) for [SOLE 2014](http://www.sole-jole.org/2014.html). 

![plot of chunk graph](figure/graph.png) 


To produce this graph, once all papers had been allocated to either poster or full paper status (as per the [preliminary program as of January 17, 2014](http://www.sole-jole.org/2014Program.html)), we  read in [the data](SOLE2014_accepted.csv) using R:

```r
# Source: titles of all accepted papers (posters and sessions) data <-
# read.csv('~/Downloads/Lars Copy of Submissions2014_working_copy -
# Master.csv')
data <- read.csv("SOLE2014_accepted.csv")
# accepted may have been a subset
accepted <- na.omit(subset.data.frame(data, Accept_Poster == 1 | Accept_Poster == 
    2, c(Title)))
```

We then used the _R text mining library_  to clean and parse the titles:

```r
library(tm)
doc.vec <- VectorSource(t(accepted))
doc.corpus <- Corpus(doc.vec)
doc.corpus <- tm_map(doc.corpus, tolower)
doc.corpus.nw <- tm_map(doc.corpus, stripWhitespace)
doc.corpus <- tm_map(doc.corpus, removePunctuation)
doc.corpus <- tm_map(doc.corpus, removeNumbers)
doc.corpus <- tm_map(doc.corpus, removeWords, stopwords("english"))
TDM <- TermDocumentMatrix(doc.corpus)
try_max <- 65
try_five <- 30
```

which generated a "corpus" of documents:

```r
summary(doc.corpus)
```

```
## A corpus with 343 text documents
## 
## The metadata consists of 2 tag-value pairs and a data frame
## Available tags are:
##   create_date creator 
## Available variables in the data frame are:
##   MetaID
```


In fact, we lied somewhat above: we did not show **1183** words, but rather, for the sake of clarity, restricted ourselves to the **150** words with at least 4 mentions in the (cleaned) corpus. If we had not, we would have obtained  the following graph:


```r
myscale <- c(5, 0.1)
# if producing the webpage, you need to comment out the following two lines
# png('graphing.SOLE2014.all_titles.png',height=1600,width=1600,res=600)
# myscale <- c(2,.1)
wordcloud(rownames(as.matrix(TDM)), rowSums(as.matrix(TDM)), scale = myscale, 
    min.freq = 1, color = palette, rot.per = 0.3, random.order = FALSE)
```

![plot of chunk graph2](figure/graph2.png) 

For the curious, while the most frequent word is **evidence**, the top **5** are:

```
##   findFreqTerms(TDM, try_five)
## 1                      effects
## 2                     evidence
## 3                        labor
## 4                       market
## 5                         wage
```


Because there is a fair bit of randomness in the graph generation, we ran through the last R chunk a few times, generating a few alternate, high-resolution versions:

 * [Attempt 1](graphing.SOLE2014.accepted_posters.picture.MANUAL.png)
 * [Attempt 2](graphing.SOLE2014.all_titles.HIRES1.png)
 * [Attempt 3](graphing.SOLE2014.all_titles.HIRES2.png)
 * [Attempt 4](graphing.SOLE2014.all_titles.HIRES3.png)

Also, we did play around with restricting the graphic to only those papers accepted into the poster session, see the [R Markdown document](graphing.SOLE2014.posters_only.Rmd) ([Markdown version](graphing.SOLE2014.posters_only.md)) ([HTML version](graphing.SOLE2014.posters_only.html)).

*******************
 * The code behind this endeavor is available at [https://bitbucket.org/vilhuberl/sole2014](https://bitbucket.org/vilhuberl/sole2014).
 * This document was produced using

```r
R.Version()
```

```
## $platform
## [1] "x86_64-suse-linux-gnu"
## 
## $arch
## [1] "x86_64"
## 
## $os
## [1] "linux-gnu"
## 
## $system
## [1] "x86_64, linux-gnu"
## 
## $status
## [1] ""
## 
## $major
## [1] "3"
## 
## $minor
## [1] "1.0"
## 
## $year
## [1] "2014"
## 
## $month
## [1] "04"
## 
## $day
## [1] "10"
## 
## $`svn rev`
## [1] "65387"
## 
## $language
## [1] "R"
## 
## $version.string
## [1] "R version 3.1.0 (2014-04-10)"
## 
## $nickname
## [1] "Spring Dance"
```

```r
Sys.info()
```

```
##                                                sysname 
##                                                "Linux" 
##                                                release 
##                                    "3.11.10-7-desktop" 
##                                                version 
## "#1 SMP PREEMPT Mon Feb 3 09:41:24 UTC 2014 (750023e)" 
##                                               nodename 
##                                             "zotique2" 
##                                                machine 
##                                               "x86_64" 
##                                                  login 
##                                             "vilhuber" 
##                                                   user 
##                                             "vilhuber" 
##                                         effective_user 
##                                             "vilhuber"
```

