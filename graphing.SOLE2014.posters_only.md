SOLE 2014 posters as a picture
======================




Or: the power of **evidence**.

A picture is worth a thousand words. Or in this case 697 words - the number of distinct words in titles of the 148 papers accepted in the poster session at SOLE 2014. 

![plot of chunk graph](figure/graph.png) 


To produce this graph, once all papers had been allocated, we read in [the data](SOLE2014_accepted.csv) using R:

```r
# Source: titles of all accepted papers (posters and sessions)
data <- read.csv("SOLE2014_accepted.csv")
# subset to posters
posters <- na.omit(subset.data.frame(data, Accept_Poster == 2, c(Title)))
```

We then used the _R text mining library_  to clean and parse the titles:

```r
library(tm)
doc.vec <- VectorSource(t(posters))
doc.corpus <- Corpus(doc.vec)
doc.corpus <- tm_map(doc.corpus, tolower)
doc.corpus.nw <- tm_map(doc.corpus, stripWhitespace)
doc.corpus <- tm_map(doc.corpus, removePunctuation)
doc.corpus <- tm_map(doc.corpus, removeNumbers)
doc.corpus <- tm_map(doc.corpus, removeWords, stopwords("english"))
TDM <- TermDocumentMatrix(doc.corpus)
# this should really be dynamically determined, but I was lazy
try_max <- 25
try_five <- 13
min_freq <- 4
```

which generated a "corpus" of documents:

```r
summary(doc.corpus)
```

```
## A corpus with 148 text documents
## 
## The metadata consists of 2 tag-value pairs and a data frame
## Available tags are:
##   create_date creator 
## Available variables in the data frame are:
##   MetaID
```


In fact, we lied somewhat above: we did not show **697** words, but rather, for the sake of clarity, restricted ourselves to the **53** words with at least 4 mentions in the (cleaned) corpus. If we had not, we would have obtained  the following graph:

![plot of chunk graph2](figure/graph2.png) 

For the curious, while the most frequent word is **evidence**, the top **5** are:

```
##   findFreqTerms(TDM, try_five)
## 1                    education
## 2                     evidence
## 3                        labor
## 4                       market
## 5                         wage
```


*******************
 * The code behind this endeavor is available at [https://bitbucket.org/vilhuberl/sole2014](https://bitbucket.org/vilhuberl/sole2014).
 * This document was produced using

```r
R.Version()
```

```
## $platform
## [1] "x86_64-suse-linux-gnu"
## 
## $arch
## [1] "x86_64"
## 
## $os
## [1] "linux-gnu"
## 
## $system
## [1] "x86_64, linux-gnu"
## 
## $status
## [1] ""
## 
## $major
## [1] "3"
## 
## $minor
## [1] "1.0"
## 
## $year
## [1] "2014"
## 
## $month
## [1] "04"
## 
## $day
## [1] "10"
## 
## $`svn rev`
## [1] "65387"
## 
## $language
## [1] "R"
## 
## $version.string
## [1] "R version 3.1.0 (2014-04-10)"
## 
## $nickname
## [1] "Spring Dance"
```

```r
Sys.info()
```

```
##                                                sysname 
##                                                "Linux" 
##                                                release 
##                                    "3.11.10-7-desktop" 
##                                                version 
## "#1 SMP PREEMPT Mon Feb 3 09:41:24 UTC 2014 (750023e)" 
##                                               nodename 
##                                             "zotique2" 
##                                                machine 
##                                               "x86_64" 
##                                                  login 
##                                             "vilhuber" 
##                                                   user 
##                                             "vilhuber" 
##                                         effective_user 
##                                             "vilhuber"
```

