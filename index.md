Visualizing SOLE 2014
==================

These pages are an effort to visualize the themes in papers and presentations at [SOLE 2014](http://www.sole-jole.org/2014.html). The initial thought was that this could help organize sessions (we ended up doing it the old-fashioned way, with some help from the word frequencies), but we thought the pictures were pretty enough to warrant (a) inclusion into the program (b) an explanation of how we generated them.

Key documents
------------

 * [graphing.SOLE2014.accepted_posters.html](graphing.SOLE2014.accepted_posters.html): the main document, using all occurrences of words in titles of full papers and posters (Sources: [graphing.SOLE2014.accepted_posters.Rmd](graphing.SOLE2014.accepted_posters.Rmd) and its output [graphing.SOLE2014.accepted_posters.md](graphing.SOLE2014.accepted_posters.md))
 * [graphing.SOLE2014.posters_only.html](graphing.SOLE2014.posters_only.html): an earlier attempt at using just the words in titles of posters (Sources: [graphing.SOLE2014.posters_only.Rmd](graphing.SOLE2014.posters_only.Rmd) and its output [graphing.SOLE2014.posters_only.md](graphing.SOLE2014.posters_only.md))

Tools
-----
 * [RStudio](http://rstudio.com), a really nice IDE for R and other things
 * [R Markdown](http://rmarkdown.rstudio.com/) ([how to use it in RStudio](http://www.rstudio.com/ide/docs/authoring/using_markdown))
 * [knitr](http://yihui.name/knitr/)
 * R libraries:
    - library([tm](http://cran.r-project.org/web/packages/tm/index.html)) 
    - library([wordcloud](http://cran.r-project.org/web/packages/wordcloud/index.html))

Sources
------
The code behind this endeavor is available at [https://bitbucket.org/vilhuberl/sole2014](https://bitbucket.org/vilhuberl/sole2014), and you can find this page at [http://www.vrdc.cornell.edu/sole2014/](http://www.vrdc.cornell.edu/sole2014/).

Author
-----
[Lars Vilhuber](mailto:lars.vilhuber@cornell.edu), [Cornell University](http://www.cornell.edu) [Economics Department](http://economics.cornell.edu) and [Labor Dynamics Institute](http://www.ilr.cornell.edu/ldi/).

